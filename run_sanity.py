#!/usr/bin/env python

import os

job_list = [
"bnxt-roce.xml",
"cxgb4-iw.xml",
"hfi1-opa.xml",
"mlx4-ib.xml",
"mlx4-roce.xml",
"mlx5-ib.xml",
"mlx5-roce.xml",
"qedr-iw.xml",
"irdma-roce.xml"
]

whit_board = "RDMA v6.5 update"
distro_name = "RHEL-8.9.0-updates-20230828.40"
kernel_build = "511.el8.8.10_core_1032_6.5.ge214"

for job in job_list:
    os.system("git checkout -- %s" % job)
    os.system("sed -i 's,| RDMA sanity <,| RDMA sanity | %s<,g' %s" % (whit_board, job))
    os.system("sed -i 's,<distro_name op=\"=\" value=\"\"/>,<distro_name op=\"=\" value=\"%s\"/>,g' %s" % (distro_name, job))
    os.system("sed -i 's,KERNELBUILD,%s,g' %s" %  (kernel_build, job))
    os.system("bkr job-submit %s" % (job))
